// this program simulates a data race condition where two processes are
// computing based off a single variable:
//
// first the value is read in a temporary variable, then a computation is
// done on that variable (here simply adding one). The "global" variable is
// then updated with this new value.

// Expected behaviour; V will be 102 and A will be set to 1.

// Possible behaviour; par 1 reads V and sets X to 100, then par 2 is fully
// executed and V is set to 101. par 1 then completes its computation and V is
// set 101.

V = 100;

par {
	X = V;
	X += 1;
	V = X;
}
{
	Y = V;
	Y += 1;
	V = Y;
}

A = -1;

if V == 102 {
	A = 1;
}
else {
	A = 0;
}

