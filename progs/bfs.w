// A -------- B
// | \\     / |
// |  C---D   |
// |  |   |   |
// |  E---F   |
// | /     \\ |
// G ---------H

// 1
A = 2;

// 2
B = 5;

// 3
C = 3;

// 4
D = 11;

// 5
E = 14;

// 6
F = 9;

// 7
G = 1;

// 8
H = 8;

// search number
S = 5;
// node
N = -1;

if A == S {
	N = 1;
}
else {
	if B == S {
		N = 2;
	}
	else {
		if C == S {
			N = 3;
		}
		else {
			if G == S {
				N = 7;
			}
			else {
				if D == S {
					N = 4;
				}
				else {
					if H == S {
						N = 8;
					}
					else {
						if E == S {
							N = 5;
						}
						else {
							if F == S {
								N = 6;
							}
						}
					}
				}
			}
		}
	}
}

