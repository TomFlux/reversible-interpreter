//        A                4        
//       / \              / \       
//      /   \            /   \      
//     /     \          /     \     
//    B       C        5       9    
//   / \     / \      / \     / \   
//  D   E   F   G    8  23   0   0  

// numbered from A = 1, G = 7
// zero indicates unset
A = 4;
B = 5;
C = 9;
D = 8;
E = 23;
F = 0;
G = 0;

// set flag
S = 0;

// node inserted at
N = 0;
// number to insert
I = 2;

// find next available node
if D == 0 {
	if !B == 0 {
		if !A == 0 {
			N = 4;
			D = I;
			S = 1;
		}
	}
}

if S == 0 {
	if E == 0 {
		if !B == 0 {
			if !A == 0 {
				N = 5;
				E = I;
				S = 1;
			}
		}
	}
}

if S == 0 {
	if F == 0 {
		if !C == 0 {
			if !A == 0 {
				N = 6;
				F = I;
				S = 1;
			}
		}
	}
}

if S == 0 {
	if G == 0 {
		if !C == 0 {
			if !A == 0 {
				N = 7;
				G = I;
				S = 1;
			}
		}
	}
}

if S == 0 {
	if B == 0 {
		if !A == 0 {
			N = 2;
			B = I;
			S = 1;
		}
	}
}

if S == 0 {
	if C == 0 {
		if !A == 0 {
			N = 3;
			C = I;
			S = 1;
		}
	}
}

if S == 0 {
	if A == 0 {
		N = 1;
		A = I;
		// dont set A, assume we have an empty tree
	}
}

if S == 1 {
	if N == 2 {
		if B < A {
			T = B;
			B = A;
			A = T;
		}
	}
	if N == 3 {
		if C < A {
			T = C;
			C = A;
			A = T;
		}
	}
	if N == 4 {
		if D < B {
			T = D;
			D = B;
			B = T;
			if B < A {
				T = B;
				B = A;
				A = T;
			}
		}
	}
	// 31
	if N == 5 {
		// 32
		if E < B {
			T = E;
			E = B;
			B = T;
			// 33
			if B < A {
				T = B;
				B = A;
				A = T;
			}
		}
	}
	// 34
	if N == 6 {
		// 35
		if F < C {
			T = F;
			F = C;
			C = T;
			// 36
			if C < A {
				T = C;
				C = A;
				A = T;
			}
		}
	}
	// 37
	if N == 7 {
		if G < C {
			T = G;
			G = C;
			C = T;
			if C < A {
				T = C;
				C = A;
				A = T;
			}
		}
	}
}
