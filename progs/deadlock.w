// this is an implementation of "The Dining Philosopher Scenario"
// https://docs.oracle.com/cd/E19205-01/820-0619/geosb/index.html
// 
// Here, two philosopher are sat at the table thinking for a set (or random)
// time. This is represented by the variables W and X in this program. After
// the philosopher have finished thinking, they wish to eat; however, they each
// only own one chopstick, so must use the other's to eat. Eating takes a set
// amount of time, here represented by the variables F and G. If the other's
// chopstick is in use, then they must wait until it is free to eat. After they
// eat they release the chopstick.
//
// A "deadlock" happens when each philosopher finishes thinking at the same
// time. They grab their own chopstick, then wait until the other finishes
// eating. But as they are both waiting for the other to finish eating, neither
// will and a deadlock happens.

// philosopher one
W = 10;
F = 0;

// philosopher two 
X = 3;
G = 0;

// chopstick one
C = 0;

// chopstick two
D = 0;

par {
	// philosopher one
	while W > 0 {
		W -= 1;
	}

	C = 1;

	while D == 1 {
		Z = 0;
	}

	D = 1;

	while F < 4 {
		F += 1;
	}

	C = 0;
	D = 0;
}
{
	// philosopher two
	while X > 0 {
		X -= 1;
	}

	D = 1;

	while C == 1 {
		Z = 0;
	}

	C = 1;

	while G < 4 {
		G += 1;
	}

	D = 0;
	C = 0;
}


