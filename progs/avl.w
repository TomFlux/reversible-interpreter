// This is an implmentaiton of inserting into a tree using DFS to insert the
// node; then using AVl logic to balance the tree. This should work for any
// tree given to it

//        A                4        
//       / \              / \       
//      /   \            /   \      
//     /     \          /     \     
//    B       C        5       0    
//   / \     / \      / \     / \   
//  D   E   F   G    0   0   0   0  

// numbered from A = 1, G = 7
// captials are node values
// lower case are branching factor
// zero indicates unset
A = 4;
a = 0;

B = 5;
b = 0;

C = 0;
c = 0;

D = 0;
d = 0;

E = 0;
e = 0;

F = 0;
f = 0;

G = 0;
g = 0;

// node to insert
I = 11;

// set flag
S = 0;

// parent node inserted at
N = -1;
// inserted left (-1) or right (1)
M = 0;

//'===========`
//' Insertion `
//'===========`

//        A                4        
//       / \              / \       
//      /   \            /   \      
//     /     \          /     \     
//    B       C        5       0    
//   / \     / \      / \     / \   
//  D   E   F   G    11  0   0   0  

if A == 0 {
	A = I;
	S = 1;
}

if S == 0 {
	if B == 0 {
		B = I;
		S = 1;
		N = 1;
		M = -1;
	}
}

if S == 0 {
	if D == 0 {
		D = I;
		S = 1;
		N = 2;
		M = -1;
	}
}

if S == 0 {
	if E == 0 {
		E = I;
		S = 1;
		N = 2;
		M = 1;
	}
}

if S == 0 {
	if C == 0 {
		C = I;
		S = 1;
		N = 1;
		M = 1;
	}
}

if S == 0 {
	if F == 0 {
		F = I;
		S = 1;
		N = 3;
		M = -1;
	}
}

if S == 0 {
	if G == 0 {
		G = I;
		S = 1;
		N = 3;
		M = -1;
	}
}

//'=========`
//' Find BF `
//'=========`

//        A                5        
//       / \              / \       
//      /   \            /   \      
//     /     \          /     \     
//    B       C        11      4    
//   / \     / \      / \     / \   
//  D   E   F   G    0   0   0   0  


L = 0;
R = 0;

if !D == 0 {
	L = 1;
}

if !E == 0 {
	R = 1;
}

b = R - L;

L = 0;
R = 0;

if !F == 0 {
	L = 1;
}

if !G == 0 {
	R = 1;
}

c = R - L;

L = 0;
R = 0;

if !B == 0 {
	L += 1;

	if !D == 0 {
		L += 1;
	}
	else {
		if !E == 0 {
			L += 1;
		}
	}
}

if !C == 0 {
	R += 1;	

	if !F == 0 {
		R += 1;
	}
	else {
		if !G == 0 {
			R += 1;
		}
	}
}

a = R - L;


//'==========`
//' Reblance `
//'==========`

// left or right?
if M == -1 {
	// which parent node?
	if N == 2 {
		// is it imbalanced
		if C == 0 {
			C = A;	
			A = B;
			B = D;
			D = 0;
		}
	}

	if N == 3 {
		if B == 0 {
			B = A;
			A = C;
			C = F;
			F = 0;
		}
	}
}

if M == 1 {
	if N == 2 {
		if C == 0 {
			C = A;	
			A = B;
			B = E;
			E = 0;
		}
	}

	if N == 3 {
		if B == 0 {
			B = A;
			A = C;
			C = G;
			G = 0;
		}
	}
}


