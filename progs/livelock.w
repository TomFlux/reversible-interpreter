// this a simple livelock example
// here, it could easily occur that proceess one is executed twice, before
// process two has the chance to set L to 1 and stop process one from becoming
// and infinite loop.
//
// This is different to a deadlock, as execution is still happening, in this
// case S will indefinetly be increased


L = 0;

S = 0;

par {
	while L == 0 {
		S += 1;
	}
}
{
	if S == 10 {
		L = 1;
	}
}


