// this is a slightly contrived example of an order violation.
//
// if the programmer expected that process one will be executed before process
// two, then they could not account for the case where B does not get to 10,
// and thus C is equal to 100001 instead of 101.

A = 10;
B = 10000;

par {
	B = 10;
}
{
	A *= B;
}

C = A + 1;
