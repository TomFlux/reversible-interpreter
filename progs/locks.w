// Here, locks can be circumvented. The idea that X can only be computed on
// when S is "unlocked" (set to 0). If the value of X becomes 2 then we are
// saying it is "unsafe".

// Expected behaviour; each block waits until S is unlocked then performs
// its operation on X.

// Possible behaviour; par 1 checks, then immediately par 2 checks S. Both
// of these checks pass and X is set to 2, which is "unsafe"


// lock
S = 0;

// "safe" values are either 0 or 1
X = 0;

par {
	while S == 1 {
		W = 0;
	}

	S = 1;
	X += 1;

	S = 0;
}
{
	while S == 1 {
		W = 0;
	}

	S = 1;
	X += 1;

	S = 0;
}
