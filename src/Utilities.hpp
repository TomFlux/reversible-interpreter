#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <string>
#include <vector>
#include <filesystem>
#include <regex>
#include <algorithm>
#include <fstream>

#include "Simulator.hpp"

#include "../build/while.tab.h"
extern bool parse(std::string path);

class Utilities {
public:
	static std::vector<std::string> list_directory(const std::string& path, bool include_parent_dir);

	/*
	 * try to start parsing from here as we are already doing funky things
	 */
	static void begin_parse(const std::string& path);
};

#endif
