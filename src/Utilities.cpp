#include "Utilities.hpp"

std::vector<std::string> Utilities::list_directory(const std::string& path, bool include_parent_dir) {
	std::vector<std::string> paths = { };

	for (const auto & file : std::filesystem::directory_iterator(path)) {
		std::string this_path = file.path();
		if (!include_parent_dir) {

			std::string full_string = path;
			if (full_string[full_string.length() - 1] != '/') {
				full_string += "/";
			}

			this_path = std::regex_replace(this_path, std::regex(full_string), "");
		}

		paths.push_back(this_path);
	}

	std::sort(paths.begin(), paths.end());

	return paths;
}


void Utilities::begin_parse(const std::string& path) {
	Simulator& sim = Simulator::get_instance();

	sim.reset();
	sim.has_parsed = true;

	if (parse(path)) {
		std::string line;
		std::ifstream myfile(path);

		if (myfile.is_open()) {
			while (getline(myfile, line)) {
				sim.current_program.push_back(line + "\n");
			}
			myfile.close();
		}

		sim.generate_human_programs();
	}
}
