#include "Instruction.hpp"
#include "Simulator.hpp"
#include <iostream>

/*
 *
 *	UTILS
 *
 */

int64_t Instruction::count_to_next_false(int64_t id) {
	std::stack<bool>s(Simulator::get_instance().while_environment[id]);
	int64_t count = 0;

	while (!s.empty()) {
		if (!s.top()) {
			break;
		}

		count++;
		s.pop();
	}

	return count;
}

int64_t Instruction::perform_operations(
	std::vector<Operation>& operations
) {
	Simulator &sim = Simulator::get_instance();

	if (operations.size() == 1) {
		if (operations[0].operand.is_literal) {
			return operations[0].operand.value;
		} else {
			return sim.get_symbol_from_table(operations[0].operand.value);
		}
	}

	const auto old_ops(operations);

	std::stack<Operation::Operator> order_of_ops = { };

	Operation::Operator current_op;
	order_of_ops.push(Operation::Operator::Subtraction);
	order_of_ops.push(Operation::Operator::Addition);
	order_of_ops.push(Operation::Operator::Multiplication);
	order_of_ops.push(Operation::Operator::Division);

	while (!order_of_ops.empty()) {
		current_op = order_of_ops.top();
		order_of_ops.pop();

		for (size_t i = 1; i < operations.size(); i++) {
			auto& op = operations[i];

			if (!op.operand.is_literal) {
				op.operand.value = sim.get_symbol_from_table(op.operand.value);

				op.operand.is_literal = true;
			}

			if (op.operation == current_op) {
				auto& prev_op = operations[i - 1];

				if (!prev_op.operand.is_literal) {
					prev_op.operand.value = sim.get_symbol_from_table(prev_op.operand.value);

					prev_op.operand.is_literal = true;
				}

				switch (current_op) {
				case Operation::Operator::Addition: {
					prev_op.operand.value += op.operand.value;
					break;
				}
				case Operation::Operator::Subtraction: {
					prev_op.operand.value -= op.operand.value;
					break;
				}
				case Operation::Operator::Multiplication: {
						prev_op.operand.value *= op.operand.value;
						break;
					}
				case Operation::Operator::Division: {
					prev_op.operand.value /= op.operand.value;
					break;
				}
				default: break;
				}

				operations.erase(
					operations.begin() + i
				);
			}
		}
	}


	int64_t val = operations[0].operand.value;
	operations = old_ops;
	return val;
}

bool Instruction::evaluate_boolean(BooleanEquation &bool_eq) {
	int64_t operand_one = Instruction::perform_operations(
		bool_eq.operand_one
	);

	int64_t operand_two = Instruction::perform_operations(
		bool_eq.operand_two
	);

	bool ret = false;
	switch (bool_eq.operation) {
	case BooleanEquation::Comparison::Equals: {
		ret = operand_one == operand_two;
		break;
	}
	case BooleanEquation::Comparison::GreaterThan: {
		ret = operand_one > operand_two;
		break;
	}
	case BooleanEquation::Comparison::GreaterThanEquals: {
		ret = operand_one >= operand_two;
		break;
	}
	case BooleanEquation::Comparison::LessThan: {
		ret = operand_one < operand_two;
		break;
	}
	case BooleanEquation::Comparison::LessThanEquals: {
		ret = operand_one <= operand_two;
		break;
	}
	}

	if (bool_eq.negated) {
		ret = !ret;
	}

	return ret;
}

void Instruction::print_operations(std::ostream &os, const std::vector<Operation>& operations) {
	for (const auto& op : operations) {
		char opera = ' ';

		switch (op.operation) {
		case Operation::Operator::Addition:
			opera = '+';
			break;
		case Operation::Operator::Subtraction:
			opera = '-';
			break;
		case Operation::Operator::Multiplication:
			opera = '*';
			break;
		case Operation::Operator::Division:
			opera = '/';
			break;
		case Operation::Operator::Assignment:
			opera = '=';
			break;
		}

		os << "(" << opera << ", ";

		if (op.operand.is_literal) {
			os << op.operand.value;
		} else {
			os << (char)op.operand.value;
		}
		os << "), ";
	}
}

/*
 *
 *	ASSIGNMENT
 *
 */

enum Instruction::InstructionType I_Assignment::get_instruction_type() const {
	return Instruction::InstructionType::I_ASS;
}

// these should _never_ be called
void I_Assignment::add_executed_instruction(std::shared_ptr<Instruction> _) { }
void I_Assignment::clear_executed_instruction() { }
std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> I_Assignment::get_next_instruction() {
	return make_pair(std::nullopt, -1);
}
void I_Assignment::remove_execution() { }

void I_Assignment::check_constructive() {
    this->is_constructive_ = !std::any_of(
            this->operations.begin(), this->operations.end(),
            [&](const Operation& op) {
                return !op.operand.is_literal && (
                         op.operand.value == this->ascii_variable
                         && op.operation != Operation::Operator::Assignment
                        );
            }
            );
}

void I_Assignment::execute(const int64_t proc) {
	Simulator &sim = Simulator::get_instance();
	sim.current_line_num = this->line_num;

	if (this->operations.front().operation != Operation::Operator::Assignment)
		return;

	int64_t val = Instruction::perform_operations(
		this->operations
	);

	sim.add_symbol(
		!this->is_constructive_,
		this->ascii_variable,
		val,
		true
	);
}

void I_Assignment::rexecute(const int64_t proc) {
	Simulator &sim = Simulator::get_instance();
	sim.current_line_num = this->line_num;

	int64_t value;

	if (this->is_constructive_) {
		const auto old_op = this->operations[1].operation;

		this->operations[1].operation = Operation::get_opposite(old_op);

		value = Instruction::perform_operations(
			this->operations
		);

		this->operations[1].operation = old_op;
	} else {
		value = sim.get_symbol_from_environment(this->ascii_variable);
	}

	sim.add_symbol(
		false,
		this->ascii_variable,
		value,
		true
	);
}

std::ostream &operator<<(std::ostream &os, const I_Assignment &instr) {
	os << "{\"" << (char)instr.ascii_variable << "\", <";

	Instruction::print_operations(os, instr.operations);

	os << ">}";

	return os;
}

/*
 *
 *	CONDITIONAL
 *
 */

int64_t I_Conditional::count_ = 0;

enum Instruction::InstructionType I_Conditional::get_instruction_type() const {
	return Instruction::InstructionType::I_CON;
}

void I_Conditional::add_instruction(const bool branch, std::shared_ptr<Instruction> instr) {
	Simulator::get_instance().valid_lines.push_back(instr->line_num);
	instr->parent = std::make_optional(InstructionType::I_CON);
	if (branch) {
		this->b_true.push_back(instr);
	} else {
		this->b_false.push_back(instr);
	}
}

void I_Conditional::add_executed_instruction(std::shared_ptr<Instruction> instr) {
	if (Simulator::get_instance().conditional_environment[this->identifier].top()) {
		this->executed_b_true_.insert(
			this->executed_b_true_.begin(),
			instr
		);
	} else {
		this->executed_b_false_.insert(
			this->executed_b_false_.begin(),
			instr
		);
	}
}

void I_Conditional::remove_execution() {
	if (Simulator::get_instance().conditional_environment[this->identifier].top()) {
		if (!this->executed_b_true_.empty()) {
			this->executed_b_true_.erase(
				this->executed_b_true_.begin()
			);
		}
	} else {
		if (!this->executed_b_false_.empty()) {
			this->executed_b_false_.erase(
				this->executed_b_false_.begin()
			);
		}
	}

}

void I_Conditional::execute(const int64_t proc) {
	Simulator& sim = Simulator::get_instance();
	sim.current_line_num = line_num;
	std::vector<std::shared_ptr<Instruction>>& old_il = Scheduler::get_current_instruction_list(proc);

	bool branch = evaluate_boolean(this->bool_eq);

	if (branch) {
		old_il.insert(old_il.begin() + 1, this->b_true.begin(), this->b_true.end());
	} else {
		old_il.insert(old_il.begin() + 1, this->b_false.begin(), this->b_false.end());
	}

	sim.update_boolean_results(
		this->identifier,
		branch
	);

	// we only want to update execution order when we are not nested
	if (this->parent == std::nullopt) {
		sim.update_execution_order(proc, std::make_shared<I_Conditional>(*this));
	} else {
		sim.update_execution_order(proc);
	}
}

void I_Conditional::rexecute(const int64_t proc) {
	Simulator& sim = Simulator::get_instance();
	sim.current_line_num = line_num;
	this->clear_executed_instruction();

	std::optional<std::shared_ptr<Instruction>> last_executed = std::nullopt;

	const bool last_evaluation = sim.conditional_environment[this->identifier].top();

	if (last_evaluation) {
		if (!this->b_true.empty()) {
			last_executed = std::make_optional<std::shared_ptr<Instruction>>(this->b_true.back());
		}
	} else {
		if (!this->b_false.empty()) {
			last_executed = std::make_optional<std::shared_ptr<Instruction>>(this->b_false.back());
		}
	}

	auto& this_proc = Scheduler::get_process(proc);
	if (last_executed != std::nullopt) {
		if (last_evaluation) {
			this->executed_b_true_.insert(
				this->executed_b_true_.begin(),
				this->b_true.rbegin() + 1,
				this->b_true.rend()
			);
		} else {
			this->executed_b_false_.insert(
				this->executed_b_false_.begin(),
				this->b_false.rbegin() + 1,
				this->b_false.rend()
			);
		}

		const auto type = last_executed.value()->get_instruction_type();

		switch(type) {
		case Instruction::InstructionType::I_ASS: {
			this_proc->instruction_lists.insert(
				this_proc->instruction_lists.begin(),
				last_executed.value()
			);
			break;
		}
		case Instruction::InstructionType::I_CON: {
			if (last_evaluation) {
				this->executed_b_true_.insert(
					this->executed_b_true_.begin(),
					last_executed.value()
				);
			} else {
				this->executed_b_false_.insert(
					this->executed_b_false_.begin(),
					last_executed.value()
				);
			}
			break;
		}
		case Instruction::InstructionType::I_WHL: {
			this_proc->instruction_lists.insert(
				this_proc->instruction_lists.begin(),
				last_executed.value()
			);

			if (last_evaluation) {
				this->executed_b_true_.insert(
					this->executed_b_true_.begin(),
					last_executed.value()
				);
			} else {
				this->executed_b_false_.insert(
					this->executed_b_false_.begin(),
					last_executed.value()
				);
			}
		}
		}

		this_proc->readd_running_block(
			std::make_shared<I_Conditional>(*this)
		);

		last_executed.value()->rexecute(proc);
	} else {
		// this is the case where the branch that was executed has
		// no instructions inside
		//
		// X = 0;
		// if X == 123 {
		// 	do something
		// }
		if (this->parent != std::nullopt) {
			Scheduler::get_process(proc)->remove_execution_from_running_block();
		}

		sim.get_bool_from_conditional_environment(this->identifier);
		this_proc->instruction_lists.insert(
			this_proc->instruction_lists.begin(),
			std::make_shared<I_Conditional>(*this)
		);
	}
}

void I_Conditional::clear_executed_instruction() {
	this->executed_b_true_.clear();
	this->executed_b_false_.clear();
}

std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> I_Conditional::get_next_instruction() {
	const bool b = Simulator::get_instance().conditional_environment[this->identifier].top();

	std::vector<std::shared_ptr<Instruction>>& branch = b 
							? this->executed_b_true_ 
							: this->executed_b_false_;

	if (branch.empty()) {
		return std::make_pair(std::nullopt, b ? this->b_true.size() : this->b_false.size());
	}

	const auto next_instr = std::optional<std::shared_ptr<Instruction>>{branch.front()};

	if (next_instr.value()->get_instruction_type() == Instruction::InstructionType::I_ASS) {
		branch.erase(
			branch.begin()
		);
	}

	return std::make_pair(next_instr, branch.size());
}

bool I_Conditional::exit_conditional() const {
	if (Simulator::get_instance().conditional_environment[this->identifier].top()) {
		return this->b_true.size() == this->executed_b_true_.size();
	} else {
		return this->b_false.size() == this->executed_b_false_.size();
	}
}

std::ostream &operator<<(std::ostream &os, const I_Conditional &instr) {
	os << "{[(";

	os << instr.identifier;

	os << ") ";

	if (instr.bool_eq.negated) {
		os << "N ";
	}

	Instruction::print_operations(os, instr.bool_eq.operand_one);

	os << "] ";

	switch (instr.bool_eq.operation) {
	case BooleanEquation::Comparison::Equals:
		os << "==";
		break;
	case BooleanEquation::Comparison::GreaterThan:
		os << ">";
		break;
	case BooleanEquation::Comparison::LessThan:
		os << "<";
		break;
	case BooleanEquation::Comparison::GreaterThanEquals:
		os << ">=";
		break;
	case BooleanEquation::Comparison::LessThanEquals:
		os << "<=";
		break;
	}

	os << " [";

	Instruction::print_operations(os, instr.bool_eq.operand_two);

	os << "] T: " << instr.b_true.size() << " ";
	os << "F: " << instr.b_false.size() << "}";

	return os;
}

/*
 *
 *	WHILE
 *
 */

int64_t I_While::count_ = 0;

enum Instruction::InstructionType I_While::get_instruction_type() const {
	return Instruction::InstructionType::I_WHL;
}

void I_While::add_instruction(std::shared_ptr<Instruction> instr) {
	Simulator::get_instance().valid_lines.push_back(instr->line_num);
	instr->parent = std::make_optional(InstructionType::I_WHL);
	this->instructions.push_back(instr);
}

void I_While::remove_execution() {
	if (!this->executed_instructions_.empty()) {
		this->executed_instructions_.erase(
			this->executed_instructions_.begin()
		);
	}
}

void I_While::execute(const int64_t proc) {
	Simulator &sim = Simulator::get_instance();
	sim.current_line_num = line_num;

	std::vector<std::shared_ptr<Instruction>>& old_il = Scheduler::get_current_instruction_list(proc);

	this->clear_executed_instruction();

	Scheduler::get_process(proc)->clear_top_block_executed_instructions(this->identifier);

	bool cond = Instruction::evaluate_boolean(this->bool_eq);

	if (cond) {
		old_il.insert(old_il.begin() + 1, std::make_shared<I_While>(*this));
		old_il.insert(old_il.begin() + 1, this->instructions.begin(), this->instructions.end());
	}

	sim.update_while_results(this->identifier, cond);

	bool added = false;
	auto& this_proc = sim.scheduler->get_process(proc);
	for (const auto &instr : this_proc->executed_instructions) {
		if (instr->get_instruction_type() == Instruction::InstructionType::I_WHL) {
			std::shared_ptr<I_While> whl = std::dynamic_pointer_cast<I_While>(instr);
			if (whl->identifier == this->identifier) {
				added = true;
				break;
			}
		}
	}

	if (added) {
		sim.update_execution_order(proc);
	} else {
		if (this->parent == std::nullopt) {
			sim.update_execution_order(proc, std::make_shared<I_While>(*this));
		} else {
			sim.update_execution_order(proc);
		}
	}
}

void I_While::rexecute(const int64_t proc) {
	Simulator& sim = Simulator::get_instance();
	sim.current_line_num = line_num;

	auto& this_env = sim.while_environment[this->identifier];
	auto& this_proc = Scheduler::get_process(proc);

	this->clear_executed_instruction();

	this->executed_instructions_.insert(
		this->executed_instructions_.begin(),
		this->instructions.rbegin(),
		this->instructions.rend()
	);

	const auto this_as_instr = std::make_shared<I_While>(*this);

	this_proc->instruction_lists.insert(
		this_proc->instruction_lists.begin(),
		this_as_instr
	);

	const bool last_evaluation = this_env.top();
	this_env.pop();

	if (!last_evaluation && (!this_env.empty() && this_env.top())) {
		this_proc->readd_running_block(this_as_instr);
	} else {
		if (this->parent != std::nullopt) {
			this_proc->remove_execution_from_running_block();
		}
	}
}

std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> I_While::get_next_instruction() {
	auto& this_env = Simulator::get_instance().while_environment[this->identifier];

	if (this->executed_instructions_.empty()) {
		if (this->count_to_next_false(this->identifier) > 1) {
			/*
			 * have evaluated the bool second (or more) time
			 */
			this->executed_instructions_.insert(
				this->executed_instructions_.begin(),
				this->instructions.rbegin(),
				this->instructions.rend()
			);
		}
		this_env.pop();

		/*
		 * have evaluated the bool for the first time
		 */
		return std::make_pair(std::nullopt, this_env.empty() || !this_env.top());
	} else {
		const auto next_instr = std::optional<std::shared_ptr<Instruction>>{this->executed_instructions_.front()};

		if (next_instr.value()->get_instruction_type() == Instruction::InstructionType::I_ASS) {
			this->executed_instructions_.erase(
				this->executed_instructions_.begin()
			);
		}

		return std::make_pair(next_instr, false);
	}
}

void I_While::add_executed_instruction(std::shared_ptr<Instruction> instr) {
	this->executed_instructions_.insert(
		this->executed_instructions_.begin(),
		instr
	);
}

void I_While::clear_executed_instruction() {
	this->executed_instructions_.clear();
}

std::ostream &operator<<(std::ostream &os, const I_While &instr) {
	os << "{[(";

	os << instr.identifier;
	
	os << ") ";

	Instruction::print_operations(os, instr.bool_eq.operand_one);

	os << "] ";

	switch (instr.bool_eq.operation) {
	case BooleanEquation::Comparison::Equals:
		os << "==";
		break;
	case BooleanEquation::Comparison::GreaterThan:
		os << ">";
		break;
	case BooleanEquation::Comparison::LessThan:
		os << "<";
		break;
	case BooleanEquation::Comparison::GreaterThanEquals:
		os << ">=";
		break;
	case BooleanEquation::Comparison::LessThanEquals:
		os << "<=";
		break;
	}

	os << " [";

	Instruction::print_operations(os, instr.bool_eq.operand_two);

	os << "] ";

	os << "Is: " << instr.instructions.size() << "} ";

	return os;
}
