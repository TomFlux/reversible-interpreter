%option noyywrap
%option nounput
%{
	#include <cstdio>
	#include <string.h>
	#include <iostream>
	#include <memory>
	#include <vector>
	#include <utility>
	#include <stack>

	#include "while.tab.h"  // to get the token types from Bison
	#include "../src/Simulator.hpp"

	using namespace std;

	int line_num = 0;

	bool F_CONDITIONAL_IN_BOOL = false;
	bool F_CONDITIONAL_NEGATED = false;
	int  F_CONDITIONAL_BOOL_SIDE = 0;
	// 0: pre, 1: first, 2: second, 3: post
	int  F_PAR_BLOCK = 0;

	std::stack<std::shared_ptr<Instruction>> working_blocks = { };
	std::stack<std::shared_ptr<Instruction>> restore_blocks = { };

	std::stack<bool> working_branches = { };

	void yyreset() {  YY_FLUSH_BUFFER; }
%}
%%
[ \t]           ;
[ \t]*\/\/.*$	; // comments
\>		{ F_CONDITIONAL_BOOL_SIDE = 1; return '>'; }
\<		{ F_CONDITIONAL_BOOL_SIDE = 1; return '<'; }
\>=		{ F_CONDITIONAL_BOOL_SIDE = 1; return GREATEREQUAL ; }
\<=		{ F_CONDITIONAL_BOOL_SIDE = 1; return LESSEQUAL    ; }
==		{ F_CONDITIONAL_BOOL_SIDE = 1; return EQUALITY     ; }
\+=		{ return CPLUS    ; }
\-=		{ return CMINUS   ; }
\*=		{ return CMULTIPLY; }
\/=		{ return CDIVIDE  ; }
!		{
			F_CONDITIONAL_NEGATED = true;
			return '!';
		}
par		{
			F_PAR_BLOCK = 1;

			if (!working_branches.empty()) {
				working_branches.pop();
			}

			return PAR;
		};
while		{
			F_CONDITIONAL_IN_BOOL = true;

			// first check to see if a conditional was the last
			// thing to be matched.
			// then check if the we are nested inside of a conditional
			if (!working_branches.empty() && working_blocks.empty()) {
				working_branches.pop();
			}

			std::shared_ptr<I_While> whl_instr(new I_While(line_num));

			working_blocks.push(whl_instr);

			return WHILE; 
		};
if		{ 
			F_CONDITIONAL_NEGATED = false;
			F_CONDITIONAL_IN_BOOL = true;

			std::shared_ptr<I_Conditional> cond_instr(new I_Conditional(line_num));

			working_blocks.push(cond_instr);
			working_branches.push(true);
			
			return IF; 
		};
else		{ 

			working_blocks.push(restore_blocks.top());
			restore_blocks.pop();

			working_branches.push(false);

			return ELSE; 
		};
\+		{ return '+'; }
\-		{ return '-'; }
\*		{ return '*'; }
\/		{ return '/'; }
=		{ return '='; }
[a-zA-Z]	{
			yylval.vname = strdup(yytext);
			return VARNAME;
		}
-?[0-9]+        {
			yylval.ival = atoi(yytext);
			return INT;
		}
\;		{ return SEMICOLON; };
\{		{ return '{'; };
\}		{
		 	if (working_blocks.empty()) {
				F_PAR_BLOCK = 2;
			}
			return '}';
		};
\n		{
			++line_num;
			return END;
		}
%%
