#include "Simulator.hpp"

void Simulator::run() const {
	this->scheduler->run();
}

void Simulator::run(int64_t steps) const {
	this->scheduler->run(steps);
}

void Simulator::reverse() const {
	this->scheduler->reverse();
}

void Simulator::undo(int64_t steps) const {
	this->scheduler->reverse(steps);
}

void Simulator::reset() {
	this->parse_error = std::nullopt;

	this->symbol_table.clear();

	while (!this->execution_order.empty()) {
		this->execution_order.pop();
	}

	this->variable_environment.clear();
	this->conditional_environment.clear();
	this->while_environment.clear();

	this->scheduler->reset_to_initial();

    this->current_program.clear();
    this->valid_lines.clear();
    this->current_line_num = 0;
    this->breakpoint_line = -1;
}

void Simulator::add_symbol(
	const bool update_env,
	const int64_t ascii_value,
	const int64_t initial_value,
	const bool is_literal
) {
	if (update_env) {
		this->update_environment((char) ascii_value, this->get_symbol_from_table(ascii_value));
	}

	Operand new_symbol = {initial_value, is_literal};

	this->symbol_table[ascii_value] = new_symbol;
}

int64_t Simulator::get_symbol_from_table(const int64_t ascii_value) const {
	for (const auto &pair : this->symbol_table) {
		int64_t symbol = pair.first;
		Operand op = pair.second;
		if (symbol == ascii_value) {
			if (op.is_literal) {
				return op.value;
			} else {
				int64_t literal = get_symbol_from_table(symbol);
				return literal;
			}
		}
	}

	return 0;
}

int64_t Simulator::get_symbol_from_environment(const char assci_value) {
	int64_t val = this->variable_environment[assci_value].top();

	this->variable_environment[assci_value].pop();

	return val;
}

bool Simulator::get_bool_from_conditional_environment(const int64_t id) {
	bool val = this->conditional_environment[id].top();

	this->conditional_environment[id].pop();

	return val;
}

int64_t Simulator::get_next_execution() {
	if (this->execution_order.empty()) {
		return -2;
	}

	int64_t val = this->execution_order.top();
	this->execution_order.pop();

	return val;
}

void Simulator::generate_human_programs() {
	this->human_programs.clear();
	std::stringstream ss;
	ss << *this->scheduler->pre_process_;
	this->human_programs.push_back(ss.str());

	ss.str("");
	ss << *this->scheduler->process_one_;
	this->human_programs.push_back(ss.str());

	ss.str("");
	ss << *this->scheduler->process_two_;
	this->human_programs.push_back(ss.str());

	ss.str("");
	ss << *this->scheduler->post_process_;
	this->human_programs.push_back(ss.str());
}

void Simulator::update_environment(const char variable, const int64_t new_value) {
	this->variable_environment[variable].push(new_value);
}

void Simulator::update_boolean_results(const int64_t id, const bool res) {
	this->conditional_environment[id].push(res);
}

void Simulator::update_while_results(const int64_t id, const bool res) {
	this->while_environment[id].push(res);
}

void Simulator::update_execution_order(const int64_t proc_id, const std::shared_ptr<Instruction>& instr) {
	if (proc_id == -1) {
		this->scheduler->pre_process_->executed_instructions.insert(
			this->scheduler->pre_process_->executed_instructions.begin(),
			instr
		);
	} else if (proc_id == 0) {
		this->scheduler->process_one_->executed_instructions.insert(
			this->scheduler->process_one_->executed_instructions.begin(),
			instr
		);
	} else if (proc_id == 1) {
		this->scheduler->process_two_->executed_instructions.insert(
			this->scheduler->process_two_->executed_instructions.begin(),
			instr
		);
	} else if (proc_id == 2) {
		this->scheduler->post_process_->executed_instructions.insert(
			this->scheduler->post_process_->executed_instructions.begin(),
			instr
		);
	}

	this->execution_order.push(proc_id);
}

void Simulator::update_execution_order(const int64_t proc_id) {
	this->execution_order.push(proc_id);
}

std::ostream &operator<<(std::ostream &os, const Simulator &s) {
	os << "Symbol table:\n";

	for (auto const &pair : s.symbol_table) {
		os << "\t{" << (char)pair.first << ": <";

		if (pair.second.is_literal) {
			os << pair.second.value;
		} else {
			os << s.get_symbol_from_table(pair.second.value);
		}

		os << "> "
		   << "}\n";
	}

	os << "Symbol Environment:" << std::endl;
	for (auto const &pair : s.variable_environment) {
		os << "\t[" << (char)pair.first << "]: {";

		std::stack<int64_t> temp(pair.second);

		while (!temp.empty()) {
			os << "<" << temp.top() << ">, ";
			temp.pop();
		}

		os << "}" << std::endl;

	}

	os << "Conditional Environment:" << std::endl;
	for (auto const &pair : s.conditional_environment) {
		os << "\t[" << pair.first << "]: {";

		std::stack<bool> temp(pair.second);

		while (!temp.empty()) {
			os << "<" << (temp.top() ? "T" : "F") << ">, ";
			temp.pop();
		}

		os << "}" << std::endl;

	}

	os << "While Environment:" << std::endl;
	for (auto const &pair : s.while_environment) {
		os << "\t[" << pair.first << "]: {";

		std::stack<bool> temp(pair.second);

		while (!temp.empty()) {
			os << "<" << (temp.top() ? "T" : "F") << ">, ";
			temp.pop();
		}

		os << "}" << std::endl;

	}

	os << "Execution order (" << s.execution_order.size() << "): " << std::endl;
	os << "\t<";

	auto t(s.execution_order);

	while (!t.empty()) {
		os << t.top() << ", ";
		t.pop();
	}
	os << ">" << std::endl;

	os << "Scheduler:\n" << *s.scheduler;

	return os;
}
