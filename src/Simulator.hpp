#ifndef SIMULATOR_H_
#define SIMULATOR_H_

#include "Scheduler.hpp"

#include <algorithm>
#include <memory>
#include <unordered_map>
#include <map>
#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
#include <sstream>
#include <memory>

class Simulator {
public:
	enum Interleaving {
		RANDOM,
		USER,
		SEQUENTIAL,
	};

	static Simulator& get_instance() {
		static Simulator instance;
		return instance;
	};

	Simulator(Simulator const&) = delete;
	void operator=(Simulator const&) = delete;

	void add_symbol(
		bool update_env,
		int64_t ascii_value,
		int64_t initial_value,
		bool is_literal
	);
	int64_t get_symbol_from_table(int64_t ascii_value) const;
	int64_t get_symbol_from_environment(char assci_value);
	bool    get_bool_from_conditional_environment(int64_t id);

    int64_t get_next_execution();

	void update_boolean_results(int64_t id, bool res);
	void update_while_results(int64_t id, bool res);

	void update_execution_order(int64_t proc_id, const std::shared_ptr<Instruction>& instr);
	void update_execution_order(int64_t proc_id);

	void generate_human_programs();

	void run() const;
	void run(int64_t steps) const;
	void reverse() const;
	void undo(int64_t steps) const;

	void reset();

	std::unique_ptr<Scheduler> scheduler;
	std::map<int64_t, Operand> symbol_table;

	std::stack<int64_t> execution_order = { };

	Simulator::Interleaving interleaving = Simulator::Interleaving::RANDOM;
    bool has_parsed = false;

	std::vector<std::string> human_programs = { };

	std::optional<std::string> parse_error = std::nullopt; 

	std::map<
		char,
		std::stack<int64_t>
	>  variable_environment = { };

	std::map<
		int64_t, std::stack<bool>
	> conditional_environment = { };

	std::map<
		int64_t, std::stack<bool>
	> while_environment = { };

	uint64_t current_line_num = 0;
	int breakpoint_line = -1;
	std::vector<std::string> current_program = { };
	std::vector<uint64_t> valid_lines = { };
private:
	Simulator() {
		scheduler = std::make_unique<Scheduler>();
	}

	void update_environment(char variable, int64_t new_value);

friend std::ostream & operator<<(std::ostream & os, const Simulator& s);
};

std::ostream & operator<<(std::ostream & os, const Simulator& s);

#endif
