#ifndef INSTRUCTION_H_
#define INSTRUCTION_H_

#include "Operation.hpp"

#include <string>
#include <vector>
#include <optional>
#include <memory>
#include <regex>
#include <iostream>
#include <optional>

typedef struct BooleanEquation {
	enum class Comparison {
		Equals,
		GreaterThan,
		GreaterThanEquals,
		LessThan,
		LessThanEquals,
	};

	std::vector<Operation> operand_one;
	std::vector<Operation> operand_two;

	Comparison operation = Comparison::Equals;
	bool negated = false;
} BooleanEquation;

class Instruction {
public:
	enum class InstructionType {
		I_ASS,
		I_CON,
		I_WHL,
	};

	virtual void execute(int64_t proc) = 0;
	virtual void rexecute(int64_t proc) = 0;
	virtual enum InstructionType get_instruction_type() const = 0;
	virtual void add_executed_instruction(std::shared_ptr<Instruction> instr) = 0;
	virtual void clear_executed_instruction() = 0;
	virtual std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> get_next_instruction() = 0;
	virtual void remove_execution() = 0;

	static int64_t perform_operations(
		std::vector<Operation> &operations
	);
	static bool evaluate_boolean(BooleanEquation &bool_eq);
	static void print_operations(std::ostream &os, const std::vector<Operation>& operations);

	static int64_t count_to_next_false(int64_t id);

	// instruction from nested instructions shouldn't
	// be stored in the sim
	std::optional<InstructionType> parent = std::nullopt;
	int64_t identifier = -1;
	uint64_t line_num = -1;
};

class I_Assignment : public Instruction {
public:
	I_Assignment(const int64_t assci_v, const std::vector<Operation>& values, const uint64_t line_num) {
		this->operations = values;
		this->ascii_variable = assci_v;

		this->is_constructive_ = false;
		this->line_num = line_num;
	}

	I_Assignment(const int64_t assci_v, const std::vector<Operation>& values, const bool is_con, const uint64_t line_num) {
		this->operations = values;
		this->ascii_variable = assci_v;
		this->line_num = line_num;
        this->is_constructive_ = false;

		if (is_con) {
			this->check_constructive();
		}
	}

	I_Assignment(const I_Assignment &other)  : Instruction(other) {
		this->operations = other.operations;
		this->ascii_variable = other.ascii_variable;

		this->parent = other.parent;
		this->is_constructive_ = other.is_constructive_;

		this->line_num = other.line_num;
	}

	I_Assignment &operator=(const I_Assignment &other) {
		if (this == &other)
			return *this;

		this->operations = other.operations;
		this->ascii_variable = other.ascii_variable;

		this->parent = other.parent;
		this->is_constructive_ = other.is_constructive_;

		this->line_num = other.line_num;

		return *this;
	}

	void execute(int64_t proc) override;
	void rexecute(int64_t proc) override;
	enum InstructionType get_instruction_type() const override;

	void add_executed_instruction(std::shared_ptr<Instruction> instr) override;
	void clear_executed_instruction() override;
	std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> get_next_instruction() override;
	void remove_execution() override;

	std::vector<Operation> operations;
	int64_t ascii_variable;

private:
	bool is_constructive_;
	void check_constructive();

	friend std::ostream &operator<<(std::ostream &os, const I_Assignment &i);
};

std::ostream &operator<<(std::ostream &os, const I_Assignment &i);

class I_Conditional : public Instruction {
public:
	explicit I_Conditional(const uint64_t line_num) {
		this->identifier = I_Conditional::count_;
		I_Conditional::count_++;

		this->b_true = {};
		this->b_false = {};

		this->executed_b_true_ = { };
		this->executed_b_false_ = { };

		this->line_num = line_num + 1;
	}

	I_Conditional(const I_Conditional &other)  : Instruction(other) {
		this->bool_eq = other.bool_eq;
		this->identifier = other.identifier;
		this->b_true = other.b_true;
		this->b_false = other.b_false;

		this->executed_b_true_ = other.executed_b_true_;
		this->executed_b_false_ = other.executed_b_false_;

		this->parent = other.parent;

		this->line_num = other.line_num;
	}

	I_Conditional &operator=(const I_Conditional &other) {
		if (this == &other)
			return *this;

		this->bool_eq = other.bool_eq;
		this->identifier = other.identifier;
		this->b_true = other.b_true;
		this->b_false = other.b_false;

		this->executed_b_true_ = other.executed_b_true_;
		this->executed_b_false_ = other.executed_b_false_;

		this->parent = other.parent;

		this->line_num = other.line_num;

		return *this;
	}

	void execute(int64_t proc) override;
	void rexecute(int64_t proc) override;
	enum InstructionType get_instruction_type() const override;
	void add_executed_instruction(std::shared_ptr<Instruction> instr) override;
	void clear_executed_instruction() override;
	std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> get_next_instruction() override;
	bool exit_conditional() const;
	void remove_execution() override;

	void add_instruction(bool branch, std::shared_ptr<Instruction> instr);

	BooleanEquation bool_eq;
	std::vector<std::shared_ptr<Instruction>> b_true;
	std::vector<std::shared_ptr<Instruction>> b_false;

private:
	static int64_t count_;

	std::vector<std::shared_ptr<Instruction>> executed_b_true_;
	std::vector<std::shared_ptr<Instruction>> executed_b_false_;

	friend std::ostream &operator<<(std::ostream &os, const I_Conditional &i);
};

std::ostream &operator<<(std::ostream &os, const I_Conditional &i);

class I_While : public Instruction {
public:
	explicit I_While(const uint64_t line_num) {
		this->identifier = I_While::count_;
		I_While::count_++;

		this->instructions = { };

		this->line_num = line_num + 1;
	}

	I_While(const I_While &other)  : Instruction(other) {
		this->bool_eq = other.bool_eq;
		this->identifier = other.identifier;
		this->instructions = other.instructions;
		this->executed_instructions_ = other.executed_instructions_;
		this->parent = other.parent;
		this->line_num = other.line_num;
	}

	I_While &operator=(const I_While &other) {
		if (this == &other)
			return *this;

		this->bool_eq = other.bool_eq;
		this->identifier = other.identifier;
		this->instructions = other.instructions;
		this->executed_instructions_ = other.executed_instructions_;
		this->parent = other.parent;
		this->line_num = other.line_num;

		return *this;
	}

	void execute(int64_t proc) override;
	void rexecute(int64_t proc) override;
	enum InstructionType get_instruction_type() const override;
	void add_executed_instruction(std::shared_ptr<Instruction> instr) override;
	void clear_executed_instruction() override;
	std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> get_next_instruction() override;
	void remove_execution() override;

	void add_instruction(std::shared_ptr<Instruction> instr);

	BooleanEquation bool_eq;
	std::vector<std::shared_ptr<Instruction>> instructions;
private:
	static int64_t count_;
	std::vector<std::shared_ptr<Instruction>> executed_instructions_ = { };

	friend std::ostream &operator<<(std::ostream &os, const I_While &i);
};

std::ostream &operator<<(std::ostream &os, const I_While &i);
#endif
