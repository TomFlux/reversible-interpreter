#include "Operation.hpp"

Operation::Operation(Operation::Operator operation_, Operand operand_) {
	this->operation = operation_;
	this->operand = operand_;
}

Operation::Operation(const Operation &other) {
	this->operation = other.operation;
	this->operand = Operand{other.operand.value, other.operand.is_literal};
}

Operation &Operation::operator=(const Operation &other) {
	if (this == &other)
		return *this;
	this->operation = other.operation;

	this->operand = Operand{other.operand.value, other.operand.is_literal};

	return *this;
}

Operation::Operator Operation::get_opposite(const Operation::Operator op) {

	switch (op) {
	case Operation::Operator::Addition: return Operation::Operator::Subtraction;
	case Operation::Operator::Subtraction: return Operation::Operator::Addition;
	case Operation::Operator::Multiplication: return Operation::Operator::Division;
	case Operation::Operator::Division: return Operation::Operator::Multiplication;
	default: return Operation::Operator::Assignment;
	}

	/* if (op == Operation::Operator::Assignment) return op;
	i wish this worked...
	return (Operation::Operator) ((((int8_t) op - 1) + 2) & 3); */
}
