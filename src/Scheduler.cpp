#include "Scheduler.hpp"
#include "Simulator.hpp"
#include <iostream>
#include <memory>
#include <vector>

Process::Process(const int64_t proc_id) {
	this->instruction_lists = { };
	this->running_blocks_ = { };
	this->proc_id_ = proc_id;
}

void Process::perform_next_instruction() {
	const auto con = Instruction::InstructionType::I_CON;
	const auto whl = Instruction::InstructionType::I_WHL;

	Simulator& sim = Simulator::get_instance();

	if (this->instruction_lists.empty()) return;

	std::shared_ptr<Instruction> this_instr = this->instruction_lists.front();
	const auto instr_type = this_instr->get_instruction_type();

	this_instr->execute(this->proc_id_);

	if (instr_type == whl || instr_type == con) {

		if (this->check_push(this_instr)) {
			if (!this->running_blocks_.empty()) {
				this->running_blocks_.top()->add_executed_instruction(
					this_instr
				);
			}

			if (instr_type == con
			 || sim.while_environment[this_instr->identifier].top()) {
				this->running_blocks_.push(this_instr);
				this->running_blocks_.top()->clear_executed_instruction();
			}
		} else if (instr_type == whl && !sim.while_environment[this_instr->identifier].top()) {
			this->running_blocks_.pop();
			this->instruction_lists.erase(this->instruction_lists.begin());
			return;
		}

		if (instr_type == con) {
			const auto cond = std::dynamic_pointer_cast<I_Conditional>(
				this_instr
			);

			if (cond->exit_conditional()) {
				this->running_blocks_.pop();

				if (cond->parent != std::nullopt) {
					this->pop_conditionals();
				}
			}
		}

	} else {
		if (this_instr->parent == std::nullopt) {
			sim.update_execution_order(this->proc_id_, this_instr);
		} else {
			sim.update_execution_order(this->proc_id_);
			this->running_blocks_.top()->add_executed_instruction(this_instr);
			if (this_instr->parent == con) {
				const auto top_cond = std::dynamic_pointer_cast<I_Conditional>(
					this->running_blocks_.top()
				);

				if (top_cond->exit_conditional()) {
					this->running_blocks_.pop();

					if (top_cond->parent != std::nullopt) {
						this->pop_conditionals();
					}
				}
			}
		}
	}

	this->instruction_lists.erase(this->instruction_lists.begin());
}

void Process::pop_conditionals() {
	while (true) {
		if (this->running_blocks_.empty()) { break; }

		if (this->running_blocks_.top()->get_instruction_type()
		 != Instruction::InstructionType::I_CON) {
			break;
		}
		const auto& top_con = std::dynamic_pointer_cast<I_Conditional>(this->running_blocks_.top());

		if (top_con->exit_conditional()) {
			this->running_blocks_.pop();
		} else {
			break;
		}
	}
}

bool Process::check_push(const std::shared_ptr<Instruction>& pot_instr) const {
	if (this->running_blocks_.empty()) {
		return true;
	}

	const std::shared_ptr<Instruction> top_instr = this->running_blocks_.top();

	if (pot_instr->get_instruction_type() == Instruction::InstructionType::I_CON) {
		if (top_instr->get_instruction_type() != pot_instr->get_instruction_type()) {
			return true;
		}
		return top_instr->identifier != pot_instr->identifier;
	}

	std::stack<std::shared_ptr<Instruction>> s(this->running_blocks_);

	while (!s.empty()) {
		if (s.top()->get_instruction_type() == Instruction::InstructionType::I_WHL) {
			if (s.top()->identifier == pot_instr->identifier) {
				return false;
			}
		}

		s.pop();
	}

	return true;
}

void Process::readd_running_block(const std::shared_ptr<Instruction>& running_instr) {
	if (running_instr->parent == std::nullopt) {
		this->executed_instructions.insert(
			this->executed_instructions.begin(),
			running_instr
		);
	}

	this->running_blocks_.push(running_instr);
}

void Process::exit_loop(const bool remove_exec) {
	// we are assuming that the top of running_blocks_ is a while
	while (true) {
		const auto front_instr = this->instruction_lists.front();
		if (front_instr->get_instruction_type() == Instruction::InstructionType::I_WHL) {
			const auto this_while = std::dynamic_pointer_cast<I_While>(front_instr);
			if (this_while->identifier == this->running_blocks_.top()->identifier) {
		
				if (remove_exec) {
					this->running_blocks_.pop();

					if (this_while->parent == std::nullopt) {
						this->executed_instructions.erase(
							this->executed_instructions.begin()
						);
					} else {
						this->running_blocks_.top()->remove_execution();
					}
				}

				break;
			}
		}

		this->instruction_lists.erase(
			this->instruction_lists.begin()
		);
	}
}

void Process::exit_conditional(const ssize_t number_of_instructions) {
	Simulator& sim = Simulator::get_instance();

	const auto top_con = std::dynamic_pointer_cast<I_Conditional>(this->running_blocks_.top());

	this->instruction_lists.erase(
		this->instruction_lists.begin(),
		this->instruction_lists.begin() + number_of_instructions
	);

	this->instruction_lists.insert(
		this->instruction_lists.begin(),
		top_con
	);

	if (top_con->parent == std::nullopt) {
		this->executed_instructions.erase(
			this->executed_instructions.begin()
		);
	}

	this->running_blocks_.pop();

	if (!this->running_blocks_.empty()) {
		this->running_blocks_.top()->remove_execution();
	}

	sim.get_bool_from_conditional_environment(top_con->identifier);
	sim.current_line_num = top_con->line_num;
}

void Process::remove_execution_from_running_block() const {
	this->running_blocks_.top()->remove_execution();
}

void Process::undo_last_instruction() {
	if (this->running_blocks_.empty()) {
		std::shared_ptr<Instruction> this_instr = this->executed_instructions.front();
		this_instr->rexecute(this->proc_id_);

		if (this_instr->get_instruction_type() == Instruction::InstructionType::I_ASS) {
			this->instruction_lists.insert(
				this->instruction_lists.begin(),
				this_instr
			);
		}

		if (this_instr->parent == std::nullopt) {
			this->executed_instructions.erase(this->executed_instructions.begin());
		}
	} else {
		const auto top_instr = this->running_blocks_.top();

		// the semantics of this type are disgusting
		const std::pair<std::optional<std::shared_ptr<Instruction>>, ssize_t> res = top_instr->get_next_instruction();
		const auto maybe_next_instr = res.first;

		if (maybe_next_instr == std::nullopt) {
			if (top_instr->get_instruction_type() == Instruction::InstructionType::I_WHL) {
				this->exit_loop(res.second);
			} else if (top_instr->get_instruction_type() == Instruction::InstructionType::I_CON) {
				this->exit_conditional(res.second);
			}
		} else {
			const std::shared_ptr<Instruction>& next_instr = maybe_next_instr.value();
			next_instr->rexecute(this->proc_id_);

			const auto type = next_instr->get_instruction_type();

			if (type == Instruction::InstructionType::I_ASS) {
				this->instruction_lists.insert(
					this->instruction_lists.begin(),
					next_instr
				);
			}
		}
	}

}

void Process::add_instruction(const std::shared_ptr<Instruction> instr) {
	Simulator::get_instance().valid_lines.push_back(instr->line_num);
	this->instruction_lists.push_back(instr);
}

void Process::reset_to_initial() {
	this->instruction_lists.clear();
	this->executed_instructions.clear();
	while (!this->running_blocks_.empty()) {
		this->running_blocks_.pop();
	}
}

void Process::clear_top_block_executed_instructions(const int64_t id) const {
	if (!this->running_blocks_.empty()) {
		if (this->running_blocks_.top()->get_instruction_type() == Instruction::InstructionType::I_WHL
		 && this->running_blocks_.top()->identifier == id) {
			this->running_blocks_.top()->clear_executed_instruction();
		}
	}
	
}

void Process::clear_running_blocks() {
	while (!this->running_blocks_.empty()) {
		this->running_blocks_.pop();
	}
}

Scheduler::Scheduler() {
	this->pre_process_ = std::make_unique<Process>(-1);
	this->process_one_ = std::make_unique<Process>(0);
	this->process_two_ = std::make_unique<Process>(1);
	this->post_process_ = std::make_unique<Process>(2);

	this->current_block_ = Block::NONE;

	std::srand(std::time(nullptr));
}

void Scheduler::clear_running_blocks() const {
	this->pre_process_->clear_running_blocks();
	this->process_one_->clear_running_blocks();
	this->process_two_->clear_running_blocks();
	this->post_process_->clear_running_blocks();
}

std::vector<std::shared_ptr<Instruction>>& Scheduler::get_current_instruction_list(const int64_t proc) {
    const auto& sched = Simulator::get_instance().scheduler;

	if (proc == -1) {
		return sched->pre_process_->instruction_lists;
	} else if (proc == 0) {
		return sched->process_one_->instruction_lists;
	} else if (proc == 1) {
		return sched->process_two_->instruction_lists;
	} else if (proc == 2) {
		return sched->post_process_->instruction_lists;
	}

	throw std::invalid_argument("proc id should be between 0 and 2");
}

bool Scheduler::step() {
	switch (this->current_block_) {
	case Scheduler::Block::NONE: {
		if (!this->pre_process_->instruction_lists.empty()) {
			this->current_block_ = Scheduler::Block::PRE;
		} else if (!this->process_one_->instruction_lists.empty()
			&& !this->process_two_->instruction_lists.empty()) {
			this->current_block_ = Scheduler::Block::PAR;
		}
	}
	case Scheduler::Block::PRE: {
		if (this->pre_process_->instruction_lists.empty()) {
			this->current_block_ = Scheduler::Block::PAR;
		} else {
			this->pre_process_->perform_next_instruction();
			break;
		}
	}
	case Scheduler::Block::PAR: {
		if (this->process_one_->instruction_lists.empty()
		 && this->process_two_->instruction_lists.empty()) {
			this->current_block_ = Scheduler::Block::POST;
		} else {
			switch(Simulator::get_instance().interleaving) {
			case Simulator::Interleaving::SEQUENTIAL: {
				if (this->next_process) {
					this->process_one_->perform_next_instruction();
				} else {
					this->process_two_->perform_next_instruction();
				}
				this->next_process = !this->next_process;
				break;
			}
			case Simulator::Interleaving::RANDOM: {
				if ((rand() % 10) < 5) {
					if (!this->process_one_->instruction_lists.empty()) {
						this->process_one_->perform_next_instruction();
					} else {
						this->process_two_->perform_next_instruction();
					}
				} else {
					if (!this->process_two_->instruction_lists.empty()) {
						this->process_two_->perform_next_instruction();
					} else {
						this->process_one_->perform_next_instruction();
					}
				}
				break;
			}
			case Simulator::Interleaving::USER: {
				if (this->next_process) {
					this->process_one_->perform_next_instruction();
				} else {
					this->process_two_->perform_next_instruction();
				}
				break;
			}
			}
			break;
		}
	}
	case Scheduler::Block::POST: {
		if (this->post_process_->instruction_lists.empty()) {
			this->current_block_ = Scheduler::Block::FIN;
		} else {
			this->post_process_->perform_next_instruction();
			break;
		}
	}
	case Scheduler::Block::FIN: {
		this->clear_running_blocks();
		return false;
	}
	}

	return true;
}

bool Scheduler::undo() {
	int64_t b = Simulator::get_instance().get_next_execution();
	if (b == -2) {
		this->current_block_ = Block::NONE;
		return false;
	} else if (b == -1) {
		this->current_block_ = Block::PRE;
		this->pre_process_->undo_last_instruction();
	} else if (b == 0) {
		this->current_block_ = Block::PAR;
		this->process_one_->undo_last_instruction();
	} else if (b == 1) {
		this->current_block_ = Block::PAR;
		this->process_two_->undo_last_instruction();
	} else if (b == 2) {
		this->current_block_ = Block::POST;
		this->post_process_->undo_last_instruction();
	}

	return true;
}

void Scheduler::run(int64_t steps) {
	Simulator& sim = Simulator::get_instance();

	int64_t step = 0;

	while (step != steps) {
		if (sim.current_line_num == (uint64_t) sim.breakpoint_line) break;
		step++;
		if (!this->step()) {
			return;
		}
		std::cout << "==== NEXT INSTR ====" << std::endl << sim << std::endl << std::endl;
	}
	sim.generate_human_programs();
}

void Scheduler::run() {
	Simulator& sim = Simulator::get_instance();

	while (this->step()) {
		if (sim.current_line_num == (uint64_t) sim.breakpoint_line) break;
		std::cout << "==== NEXT INSTR ====" << std::endl << sim << std::endl << std::endl;
	}
	sim.generate_human_programs();
}

void Scheduler::reverse(int64_t steps) {
	Simulator& sim = Simulator::get_instance();
	
	int64_t step = 0;
	while (step != steps) {
		if (sim.current_line_num == (uint64_t) sim.breakpoint_line) break;
		step++;
		if (!this->undo()) {
			return;
		}
		std::cout << "==== LAST INSTR ====" << std::endl << sim << std::endl << std::endl;
	}
	sim.generate_human_programs();

	if (this->current_block_ == Scheduler::Block::FIN) {
		sim.has_parsed = false;
	}
}

void Scheduler::reverse() {
	Simulator& sim = Simulator::get_instance();

	while (this->undo()) {
		if (sim.current_line_num == (uint64_t) sim.breakpoint_line) break;
		std::cout << "==== LAST INSTR ====" << std::endl << sim << std::endl << std::endl;
	}
	sim.generate_human_programs();

	if (this->current_block_ == Scheduler::Block::FIN) {
		sim.has_parsed = false;
	}
}

void Scheduler::reset_to_initial() {
	this->pre_process_->reset_to_initial();
	this->process_one_->reset_to_initial();
	this->process_two_->reset_to_initial();
	this->post_process_->reset_to_initial();

	this->next_process = true;

	this->current_block_ = Scheduler::Block::NONE;
}

std::unique_ptr<Process>& Scheduler::get_process(const int64_t proc) {
	Simulator& sim = Simulator::get_instance();

	if (proc == -1) {
		return sim.scheduler->pre_process_;
	} else if (proc == 0) {
		return sim.scheduler->process_one_;
	} else if (proc == 1) {
		return sim.scheduler->process_two_;
	}

	return sim.scheduler->post_process_;
}

std::ostream & operator<<(std::ostream & os, const Process& p) {
	for (const auto& instr : p.instruction_lists) {
		Instruction::InstructionType instr_type = instr->get_instruction_type();

		os << " -> ";

		if (instr_type == Instruction::InstructionType::I_ASS) {
			const std::shared_ptr<I_Assignment>& i_ass = std::dynamic_pointer_cast<I_Assignment>(instr);
			os << *i_ass;
		} else if (instr_type == Instruction::InstructionType::I_CON) {
			const std::shared_ptr<I_Conditional>& i_con = std::dynamic_pointer_cast<I_Conditional>(instr);
			os << *i_con;
		} else if (instr_type == Instruction::InstructionType::I_WHL) {
			const std::shared_ptr<I_While>& i_while = std::dynamic_pointer_cast<I_While>(instr);
			os << *i_while;
		}
	}

	return os;
}

std::ostream & operator<<(std::ostream & os, const Scheduler& s)
{
	os << "Pre Process:\n" << *s.pre_process_;
	
	os << "\nProcess One:\n" << *s.process_one_ << "\nProcess Two: \n" << *s.process_two_;

	os << "\nPost Process:\n" << *s.post_process_;
	return os;
}

