#ifndef OPERATION_H_
#define OPERATION_H_

#include <cstddef>
#include <cstdint>
#include <string>

typedef struct Operand {
	int64_t value;
	bool is_literal;
} Operand;

class Operation {
public:
	enum class Operator {
		Assignment,
		Addition,
		Subtraction,
		Multiplication,
		Division,
	};

	Operation(Operator operation_, Operand operand_);
	Operation(const Operation &other);
	Operation &operator=(const Operation &other);

	static Operation::Operator get_opposite(Operator op);

	Operator operation;
	Operand operand{};
};

#endif
