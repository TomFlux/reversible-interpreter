/* https://github.com/ocornut/imgui/blob/master/examples/example_glfw_vulkan/main.cpp */

#ifndef ENGINE_H_
#define ENGINE_H_

#include "dearimgui/imgui.h"
#include "dearimgui/imgui_impl_glfw.h"
#include "dearimgui/imgui_impl_vulkan.h"

#include "Simulator.hpp"
#include "Utilities.hpp"

#include <stdio.h>          // printf, fprintf
#include <stdlib.h>         // abort
#include <string>
#define GLFW_INCLUDE_NONE
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.h>

int setup_window();

#endif
