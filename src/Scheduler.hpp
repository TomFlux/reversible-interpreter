#ifndef PROCESS_H_
#define PROCESS_H_

#include "Instruction.hpp"

#include <optional>
#include <vector>
#include <iostream>
#include <unordered_map>
#include <ctime>
#include <random>
#include <memory>

class Process {
public:
	explicit Process(int64_t proc_id);

	std::vector<std::shared_ptr<Instruction>> instruction_lists;
	std::vector<std::shared_ptr<Instruction>> executed_instructions;

	void perform_next_instruction();
	void undo_last_instruction();
	void add_instruction(std::shared_ptr<Instruction> instr);

	void reset_to_initial();

	void readd_running_block(const std::shared_ptr<Instruction>& running_instr);
	void remove_execution_from_running_block() const;
	void clear_running_blocks();
	void clear_top_block_executed_instructions(int64_t id) const;
private:
	int64_t proc_id_;
	std::stack<std::shared_ptr<Instruction>> running_blocks_;

	void exit_loop(bool remove_exec);
	void exit_conditional(ssize_t number_of_instructions);
	bool check_push(const std::shared_ptr<Instruction>& pot_instr) const;
	void pop_conditionals();

friend class Scheduler;
friend std::ostream &operator<<(std::ostream &os, const Process &p);
};

class Scheduler {
public:
	Scheduler();

	void run();
	void run(int64_t steps);
	bool step();

	void reverse();
	void reverse(int64_t steps);
	bool undo();

	void reset_to_initial();

	static std::vector<std::shared_ptr<Instruction>>& get_current_instruction_list(int64_t proc) ;
	static std::unique_ptr<Process>& get_process(int64_t proc);

	std::unique_ptr<Process> pre_process_;
	std::unique_ptr<Process> process_one_;
	std::unique_ptr<Process> process_two_;
	std::unique_ptr<Process> post_process_;

	bool next_process = true;

private:
	enum class Block {
		NONE,
		PRE,
		PAR,
		POST,
		FIN
	};

	Block current_block_;

	void clear_running_blocks() const;

	friend std::ostream &operator<<(std::ostream &os, const Scheduler &s);
};

std::ostream &operator<<(std::ostream &os, const Process &p);
std::ostream &operator<<(std::ostream &os, const Scheduler &s);

#endif
