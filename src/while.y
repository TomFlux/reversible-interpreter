%{
	#include <iostream>
	#include <cstdio>
	#include <cstdint>
	#include <vector>
	#include <unordered_map>
	#include <utility>
	#include <memory>

	#include "../src/Simulator.hpp"

	extern int yylex();
	extern int yyparse();
	extern void yyreset();
	extern FILE *yyin;
	extern int line_num;

	extern bool F_CONDITIONAL_IN_BOOL;
	extern bool F_CONDITIONAL_NEGATED;
	extern int  F_CONDITIONAL_BOOL_SIDE;
	extern int  F_PAR_BLOCK;

	extern std::stack<std::shared_ptr<Instruction>> working_blocks;
	extern std::stack<std::shared_ptr<Instruction>> restore_blocks;
	extern std::stack<bool> working_branches;

	Simulator& sim = Simulator::get_instance();

	std::vector<Operation> C_operand_one = { };
	std::vector<Operation> C_operand_two = { };

	std::vector<Operation> values;

	bool parse(std::string path);
	void yyerror(const char *s);

	void add_new_operator(Operation::Operator o) {
		if (!working_blocks.empty()) {
			if (F_CONDITIONAL_IN_BOOL) {
				if (F_CONDITIONAL_BOOL_SIDE == 0) {
					C_operand_one.back().operation = o;
				} else {
					C_operand_two.back().operation = o;
				}
			} else {
				values.back().operation = o;
			}
		} else {
			values.back().operation = o;
		}
	}

	void add_new_operand(int64_t o, bool is_literal) {
		Operand new_operand = {o, is_literal};
		Operation* new_operation = new Operation(Operation::Operator::Assignment, new_operand);

		if (!working_blocks.empty()) {
			if (F_CONDITIONAL_IN_BOOL) {
				if (F_CONDITIONAL_BOOL_SIDE == 0) {
					C_operand_one.push_back(*new_operation);
				} else if (F_CONDITIONAL_BOOL_SIDE == 1) {
					C_operand_two.push_back(*new_operation);
				}
			} else {
				values.push_back(*new_operation);
			}
		} else {
			values.push_back(*new_operation);
		}
	}

	void add_new_conidtional(BooleanEquation::Comparison type) {
		BooleanEquation this_bool = BooleanEquation {
			C_operand_one,
			C_operand_two,
			type,
			F_CONDITIONAL_NEGATED,
		};

		C_operand_one = { };
		C_operand_two = { };

		F_CONDITIONAL_IN_BOOL = false;
		F_CONDITIONAL_BOOL_SIDE = 0;

		auto parent = working_blocks.top();
		const auto instr_type = parent->get_instruction_type();

		if (instr_type == Instruction::InstructionType::I_CON) {
			std::shared_ptr<I_Conditional> i_con = std::dynamic_pointer_cast<I_Conditional>(parent);
			i_con->bool_eq = this_bool;
		} else {
			std::shared_ptr<I_While> i_whl = std::dynamic_pointer_cast<I_While>(parent);
			i_whl->bool_eq = this_bool;
		}
	}

	void link_with_parent() {
		const auto top_working = working_blocks.top();
		const auto con_type = Instruction::InstructionType::I_CON;

		if (top_working->get_instruction_type() == con_type) {
			restore_blocks.push(top_working);	
		}

		working_blocks.pop();

		if (working_blocks.empty()) {
			if (F_PAR_BLOCK == 0) {
				sim.scheduler->pre_process_->add_instruction(top_working);
			} else if (F_PAR_BLOCK == 1) {
				sim.scheduler->process_one_->add_instruction(top_working);
			} else if (F_PAR_BLOCK == 2) {
				sim.scheduler->process_two_->add_instruction(top_working);
			} else if (F_PAR_BLOCK == 3) {
				sim.scheduler->post_process_->add_instruction(top_working);
			}
		} else {
			auto parent_parent = working_blocks.top();
			const auto instr_type = parent_parent->get_instruction_type();

			if (instr_type == con_type) {
				std::shared_ptr<I_Conditional> i_con = std::dynamic_pointer_cast<I_Conditional>(parent_parent);
				i_con->add_instruction(working_branches.top(), top_working);
			} else {
				std::shared_ptr<I_While> i_whl = std::dynamic_pointer_cast<I_While>(parent_parent);
				i_whl->add_instruction(top_working);
			}
		}
	}

	void add_arithmetic(std::shared_ptr<I_Assignment> ass_instr) {
		if (working_blocks.empty()) {
			if (F_PAR_BLOCK == 0) {
				sim.scheduler->pre_process_->add_instruction(ass_instr);
			} else if (F_PAR_BLOCK == 1) {
				sim.scheduler->process_one_->add_instruction(ass_instr);
			} else if (F_PAR_BLOCK == 2) {
				sim.scheduler->process_two_->add_instruction(ass_instr);
			} else if (F_PAR_BLOCK == 3) {
				sim.scheduler->post_process_->add_instruction(ass_instr);
			}
		} else {
			auto parent = working_blocks.top();
			const auto instr_type = parent->get_instruction_type();
			if (instr_type == Instruction::InstructionType::I_CON) {
				std::shared_ptr<I_Conditional> i_con = std::dynamic_pointer_cast<I_Conditional>(parent);
				i_con->add_instruction(working_branches.top(), ass_instr);
			} else {
				std::shared_ptr<I_While> i_whl = std::dynamic_pointer_cast<I_While>(parent);
				i_whl->add_instruction(ass_instr);
			}
		}

		values.clear();
	}

	void make_constructive(const int64_t ascii, const Operation::Operator op) {
		Operand new_operand = {ascii, false};
		Operation new_operation = Operation(Operation::Operator::Assignment, new_operand);

		values.insert(
			values.begin(),
			new_operation
		);

		values[1].operation = op;

		std::shared_ptr<I_Assignment> ass_instr(new I_Assignment(ascii, values, true, line_num));

		add_arithmetic(ass_instr);
	}

%}

%union {
	int ival;
	char *vname;
	int identifier;
}

%token PAR
%token VARNAME INT
%token END SEMICOLON
%token WHILE IF ELSE EQUALITY GREATEREQUAL LESSEQUAL CID WID
%token CPLUS CMINUS CDIVIDE CMULTIPLY

%left '+' '-' '*' '/' '!'

%type <ival>       INT
%type <identifier> WID
%type <identifier> CID
%type <vname>      VARNAME

%%
structure: program
	 | par
	 | program par	 
	 | program par program
	 ;

par: PAR '{' END program '}' END '{' END program '}' END {
		F_PAR_BLOCK = 3;
     	}
program:
          statement program
	| statement
	;
statement:
	assignment
	| conditional
	| loop
	| END
assignment:
	VARNAME '=' expression SEMICOLON END
	{
		std::shared_ptr<I_Assignment> ass_instr(new I_Assignment(*$1, values, line_num));
		add_arithmetic(ass_instr);
	}
	| VARNAME CPLUS expression SEMICOLON END { make_constructive(
		*$1, 
		Operation::Operator::Addition
	); }
	| VARNAME CMINUS expression SEMICOLON END { make_constructive(
		*$1, 
		Operation::Operator::Subtraction
	); }
	| VARNAME CMULTIPLY expression SEMICOLON END { make_constructive(
		*$1, 
		Operation::Operator::Multiplication
	); }
	| VARNAME CDIVIDE expression SEMICOLON END { make_constructive(
		*$1, 
		Operation::Operator::Division
	); }
loop:
	WHILE comparison '{' END program '}' END {
		link_with_parent();
	}
conditional:
	true_branch
	| true_branch false_branch
true_branch:
	IF comparison '{' END program '}' END {
		working_branches.pop();

		link_with_parent();
	}
false_branch:
	ELSE '{' END program '}' END {
		working_blocks.pop();

		working_branches.pop();
	}
comparison:
	'!' comparison
	| expression '>'  expression
	{ add_new_conidtional(BooleanEquation::Comparison::GreaterThan); }
	| expression '<'  expression
	{ add_new_conidtional(BooleanEquation::Comparison::LessThan); }
	| expression GREATEREQUAL expression
	{ add_new_conidtional(BooleanEquation::Comparison::GreaterThanEquals); }
	| expression LESSEQUAL  expression
	{ add_new_conidtional(BooleanEquation::Comparison::LessThanEquals); }
	| expression EQUALITY expression
	{ add_new_conidtional(BooleanEquation::Comparison::Equals); }
expression:
	VARNAME   { add_new_operand((size_t) *$1, false); }
	| INT     { add_new_operand($1, true); }
	| expression '+' expression { add_new_operator(Operation::Operator::Addition); }
	| expression '-' expression { add_new_operator(Operation::Operator::Subtraction); }
	| expression '*' expression { add_new_operator(Operation::Operator::Multiplication); }
	| expression '/' expression { add_new_operator(Operation::Operator::Division); }
%%

bool parse(std::string path) {
	FILE *myfile = fopen(path.c_str(), "r");

	line_num = 0;

	if (!myfile) {
		std::cerr << "can't find: " << path << std::endl;
		return false;
	}
	yyin = myfile;

	line_num = 0;

	F_CONDITIONAL_IN_BOOL = false;
	F_CONDITIONAL_NEGATED = false;
	F_CONDITIONAL_BOOL_SIDE = 0;
	F_PAR_BLOCK = 0;

	working_blocks = { };
	restore_blocks = { };

	yyparse();
	yyreset();

	return true;
}

void yyerror(const char *s) {
	std::string error_message = "Parse error: ";
	error_message += s;
	error_message += " on line number: " ;
	error_message += std::to_string(line_num + 1);
	Simulator::get_instance().parse_error = std::optional<std::string>(error_message);
	yyreset();
}
