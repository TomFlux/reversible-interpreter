CXX = g++
CXXFLAGS = -O0 -g3 -std=c++17 -Wall
LIBS = -lvulkan -lglfw

src_dir  = src
build_dir = build
bin_dir  = bin

All: all
all: main

main: $(build_dir)/lex.yy.c $(build_dir)/while.tab.c $(build_dir)/while.tab.h $(build_dir)/Utilities.o $(build_dir)/Operation.o $(build_dir)/Scheduler.o $(build_dir)/Simulator.o $(build_dir)/Instruction.o $(build_dir)/Engine.o $(build_dir)/imgui.o $(build_dir)/imgui_demo.o $(build_dir)/imgui_draw.o $(build_dir)/imgui_impl_glfw.o $(build_dir)/imgui_impl_vulkan.o $(build_dir)/imgui_tables.o $(build_dir)/imgui_widgets.o $(build_dir)/main.o
		$(CXX) $(CXXFLAGS) $(build_dir)/while.tab.c $(build_dir)/lex.yy.c $(build_dir)/Utilities.o $(build_dir)/Operation.o $(build_dir)/Instruction.o $(build_dir)/Simulator.o $(build_dir)/Scheduler.o $(build_dir)/Engine.o $(build_dir)/imgui.o $(build_dir)/imgui_demo.o $(build_dir)/imgui_draw.o $(build_dir)/imgui_impl_glfw.o $(build_dir)/imgui_impl_vulkan.o $(build_dir)/imgui_tables.o $(build_dir)/imgui_widgets.o $(build_dir)/main.o $(LIBS) -o bin/while

$(build_dir)/Utilities.o: $(src_dir)/Utilities.cpp $(src_dir)/Utilities.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Utilities.cpp -o $(build_dir)/Utilities.o

$(build_dir)/Operation.o: $(src_dir)/Operation.cpp $(src_dir)/Operation.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Operation.cpp -o $(build_dir)/Operation.o

$(build_dir)/Scheduler.o: $(src_dir)/Scheduler.cpp $(src_dir)/Scheduler.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Scheduler.cpp -o $(build_dir)/Scheduler.o

$(build_dir)/Simulator.o: $(src_dir)/Simulator.cpp $(src_dir)/Simulator.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Simulator.cpp -o $(build_dir)/Simulator.o

$(build_dir)/Instruction.o: $(src_dir)/Instruction.cpp $(src_dir)/Instruction.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Instruction.cpp -o $(build_dir)/Instruction.o

$(build_dir)/Engine.o: $(src_dir)/Engine.cpp $(src_dir)/Engine.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/Engine.cpp -o $(build_dir)/Engine.o

$(build_dir)/imgui.o: $(src_dir)/dearimgui/imgui.cpp $(src_dir)/dearimgui/imgui.h
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui.cpp -o $(build_dir)/imgui.o

$(build_dir)/imgui_demo.o: $(src_dir)/dearimgui/imgui_demo.cpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_demo.cpp -o $(build_dir)/imgui_demo.o

$(build_dir)/imgui_draw.o: $(src_dir)/dearimgui/imgui_draw.cpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_draw.cpp -o $(build_dir)/imgui_draw.o

$(build_dir)/imgui_impl_glfw.o: $(src_dir)/dearimgui/imgui_impl_glfw.cpp $(src_dir)/dearimgui/imgui_impl_glfw.h
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_impl_glfw.cpp -o $(build_dir)/imgui_impl_glfw.o

$(build_dir)/imgui_impl_vulkan.o: $(src_dir)/dearimgui/imgui_impl_vulkan.cpp $(src_dir)/dearimgui/imgui_impl_vulkan.h
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_impl_vulkan.cpp -o $(build_dir)/imgui_impl_vulkan.o

$(build_dir)/imgui_tables.o: $(src_dir)/dearimgui/imgui_tables.cpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_tables.cpp -o $(build_dir)/imgui_tables.o

$(build_dir)/imgui_widgets.o: $(src_dir)/dearimgui/imgui_widgets.cpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/dearimgui/imgui_widgets.cpp -o $(build_dir)/imgui_widgets.o

$(build_dir)/main.o: $(src_dir)/main.cpp $(src_dir)/main.hpp
	$(CXX) $(CXXFLAGS) -c $(src_dir)/main.cpp -o $(build_dir)/main.o

$(build_dir)/while.tab.c $(build_dir)/while.tab.h: $(src_dir)/while.y
	bison -d $(src_dir)/while.y -b $(build_dir)/while

$(build_dir)/lex.yy.c: $(src_dir)/while.l $(build_dir)/while.tab.h
	flex -o $(build_dir)/lex.yy.c $(src_dir)/while.l

clean:
	find $(build_dir) -not -name "imgui*" -type f -exec rm -f {} \; && rm -f $(bin_dir)/while

deepclean:
	rm -f $(build_dir)/* $(bin_dir)/while
