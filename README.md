# Simulation of Computation for Reversible Debugging

### List of Files


### Requirements

These are versions that are known to work.:

+ `flex`: 2.6.4
+ `bison`: 3.8.2
+ `vulkan sdk`: 1.2.204
+ `glfw`: 3.3.6

Other requirements: `make`, `g++` & `git`

### Installation

These instructions are for systems Debian based distributions (including ubuntu etc.). There should be nothing that prevents compiling on Windows if all of the above requirements are installed correctly.

1. `sudo apt install git make g++ flex bison libglfw3 libglfw3-dev vulkan-tools libvulkan-dev vulkan-validationlayers-dev spirv-tools`

*Installing `vulkan` can be non-trivial, [here](https://vulkan-tutorial.com/Development_environment#page_Linux) is the official guide for installation.*

2. `git clone https://campus.cs.le.ac.uk/gitlab/ug_project/21-22/tf119 && cd tf119`

3. `mkdir -p bin build && make -j$(nproc)`

4. `sudo chmod +x bin/while && bin/while`

### Writing Programs

**This program requires the `progs` directory is present at the same level where the program is being executed from**.
Therefore, it recommended to run the program via the `bin/while` command, rather than changing directory to `bin` and
running the executable from there.

I have chosen to use `.w` as the file extension, although any text file can be loaded into the simulator.

### List of Files
*Top level files*
+ Makefile
+ README.md
+ .gitignore
+ LICENSE
---
*These are the main implementation files*
+ src/Engine.{hc}pp
+ src/Instruction.{hc}pp
+ src/main.{hc}pp
+ src/Operation.{hc}pp
+ src/Scheduler.{hc}pp
+ src/Simulator.{hc}pp
+ src/Utilities.{hc}pp
---
*From [Dear Imgui](https://github.com/ocornut/imgui) repository*
+ src/dearimgui/imconfig.h
+ src/dearimgui/imgui.cpp
+ src/dearimgui/imgui_demo.cpp
+ src/dearimgui/imgui_draw.cpp
+ src/dearimgui/imgui.h
+ src/dearimgui/imgui_impl_glfw.{h, src/dearimgui/cpp}
+ src/dearimgui/imgui_impl_vulkan.{h, src/dearimgui/cpp}
+ src/dearimgui/imgui_internal.h
+ src/dearimgui/imgui_tables.cpp
+ src/dearimgui/imgui_widgets.cpp
+ src/dearimgui/imstb_rectpack.h
+ src/dearimgui/imstb_textedit.h
+ src/dearimgui/imstb_truetype.h
---
*Example programs for my simulator*
+ airline_simple.w
+ airline.w
+ avl.w
+ bfs.w
+ binary_search.w
+ bubble-2.w
+ bubble.w
+ comments.w
+ cond_in_whl.w
+ conditional.w
+ constr.w
+ deadlock.w
+ empty.w
+ is_even.w
+ is_lit.w
+ kd-tree.w
+ livelock.w
+ locks.w
+ min_heap.w
+ neg.w
+ nest_cond.w
+ nested_if.w
+ nested.w
+ nested_while.w
+ order_violation.w
+ par.w
+ post.w
+ read_race.w
+ rev_par.w
+ rev.w
+ seq_cond.w
+ seqwhile.w
+ syntax.w
+ to_human.w
+ while_in_simple.w
+ whl_in_con.w
+ whl.w